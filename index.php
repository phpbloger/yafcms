<?php
define('APPLICATION_PATH', dirname(__FILE__));
define('DS', DIRECTORY_SEPARATOR);
define('SITE_DIR',__DIR__. '/' );
define("SITE_NAME",basename(__DIR__));
define('TEMPLATE_ADMIN_PATH', APPLICATION_PATH.DS.'application'.DS.'modules'.DS.'Admin'.DS.'views'.DS);
$application = new \Yaf\Application( APPLICATION_PATH . "/conf/application.ini");
$application->bootstrap()->run();
