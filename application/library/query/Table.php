<?php
/**
 * User: tangyijun
 * Date: 2019-02-21
 * Time: 14:34
 */
namespace Query;

class Table extends \Cache\Cache
{
    public $table;
    public $operator = ['>' => '>', '>=' => '>=', '<' => '<', '<=' => '<=', '!=' => '!=', 'LIKE' => 'LIKE', 'NOT LIKE' => 'NOT LIKE', 'IN' => 'IN', 'NOT IN' => 'NOT IN'];
    public $selectExpr = ['DISTINCT', 'DISTINCTROW', 'SQL_CACHE', 'SQL_NO_CACHE', 'SQL_CALC_FOUND_ROWS'];
    public $fields;


    /**
     * Query constructor.
     * @param $table
     */
    public function __construct($table)
    {
        $this->table = $table;
        $data = \PdoConnect::getInstance()->fetchAll('DESC ' . $this->table);
        $this->fields = array_combine(array_column($data, 'Field'), $data);
    }

    /**
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function read($params = [])
    {
        $sql = "SELECT ";
        $params += ['selectExpr' => null, 'COLUMN' => ['id'], 'WHERE' => null, 'GROUP' => null, 'ORDER' => null, 'OFFSET' => null, 'LIMIT' => null, 'CALLBACK' => 'fetchAll', 'TYPE' => \PDO::FETCH_ASSOC];

        //查询表达式
        if (is_array($params['selectExpr']) && !empty($param['selectExpr'])) {
            $sql .= join(' ', array_intersect($param['selectExpr'], $this->selectExpr));
        }

        //查询列
        if(is_array($params['column']) && !empty($params['column']))
        {
            $sql .= ' `'.join('`,`',$params['column']).'`';
        }elseif(is_string($params['column']) &&  !empty($params['column'])){
            $sql .= $params['column'];
        }else{
            $sql .= " * ";
        }
        $sql .= ' FROM `'.$this->table.'`';
        //查询条件
        $where = $parameters = [];
        if (is_array($params['where']) && !empty($params['where'])) {
            foreach ($params['where'] as $field => $data) {
                if (is_array($data)) {
                    foreach ($data as $operator => $value) {
                        $where[] = '`'.$field.'` '.$this->operator[$operator].' ('.implode(',', array_fill(0, count($value), '?')).')';
                        $parameters = array_merge($parameters, $value);
                    }
                } else {
                    $where[] = '`'.$field.'`=?';
                    $parameters[] = $data;
                }
            }
            $sql .= ' WHERE '.join(' AND ', $where);
        }
        if (is_array($params['group']) && !empty($params['group'])) {
            $group = [];
            foreach ($params['group'] as $field => $expr) {
                $group[] = '`'.$field.'` '.$expr;
            }
            $sql .= ' GROUP BY '.join(',', $group);
        }

        //排序
        if (is_array($params['order']) && !empty($params['order'])) {
            $order = [];
            foreach ($params['order'] as $field => $expr) {
                $order[] = '`'.$field.'` '.$expr;
            }
            $sql .= ' ORDER BY '.join(',', $order);
        }

        //分页
        if ($params['offset'] >= 0 && $params['limit'] > 0 ) {
            $sql .= ' LIMIT '.(int) $params['offset'].', '.(int) $params['limit'];
        }
        //sql字段名注入检查
        preg_match_all('/`(\w+)`/', $sql, $matches);
        $whiteField = $this->fields + [$this->table => ['Field' => $this->table]];
        foreach ($matches[1] as $field) {
            if (!isset($whiteField[$field])) {
                throw new \Exception('db hack~');
            }
        }
        return \PdoConnect::getInstance()->{$params['CALLBACK']}($sql, $parameters, (int) $params['TYPE']);
    }
}