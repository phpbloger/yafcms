<?php
/**
 * User: tangyijun
 * Date: 2019-03-13
 * Time: 15:59
 */
namespace tool;
class Log extends \Cache\Cache
{
    /**
     * @param $data
     * @param string $type 1:debug,2:info,4:warn,8:error
     * @param string $file_name
     * @param bool $cut_by_dt 是否按日期指定文件名,默认false
     * @param bool $show_timestamp
     * @param string $sep
     * @return bool|int|void
     *
     */
    public function write( $data, $type = '' , $file_name='', $cut_by_dt = false, $show_timestamp = true, $sep = "\t" ) {
        $config = \Yaf\Application::app()->getConfig();
        $now        = time();
        $date       = date('Y-m-d H:i:s', $now);
        $result     = [$date];
        if ( $show_timestamp ) $result[] = $now;

        if ( !empty($type) ) $result[] = $type;
        foreach ( $data as $item ) {
            $result[] = str_replace("\t", '', $item);
        }
        $txt         = implode($sep, $result ) . PHP_EOL;
        $file_output = !empty( $cut_by_dt ) ?  ( $file_name . '_' . date('Y-m-d') . '.log' ) : ( $file_name . '.log' );
        $path        = $config->logs['path'] . SITE_NAME  . DS ;
        if ( !is_dir( $path) ) mkdir( $path,  0755, true);
        $ret = file_put_contents( $path. $file_output, $txt , FILE_APPEND );
        return $ret;
    }

    public function error($msg,$file_name = '',$cut_by_dt = false)
    {
        $debugInfo = debug_backtrace();
        $stack = "[";
        foreach($debugInfo as $key => $val){
            if(array_key_exists("file", $val)){
                $stack .= ",file:" . $val["file"];
            }
            if(array_key_exists("line", $val)){
                $stack .= ",line:" . $val["line"];
            }
            if(array_key_exists("function", $val)){
                $stack .= ",function:" . $val["function"];
            }
        }
        $stack .= "]";
        $this->write([$stack.$msg],8,$file_name,$cut_by_dt);
    }
}