<?php
/**
 * User: tangyijun
 * Date: 2019-02-22
 * Time: 10:48
 */
namespace Tool;
class Redis extends \Cache\Cache
{
    public $redis;
    public function __construct($name = 'default')
    {
        $this->redis = new \Redis();
        $arr_config = \Yaf\Application::app()->getConfig()->redis[$name];
        if($arr_config['pconnect']){
            $this->redis->pconnect($arr_config['host'], $arr_config['port'], $arr_config['timeout']);
        }else{
            $this->redis->connect($arr_config['host'], $arr_config['port'], $arr_config['timeout']);
        }
        if (!empty($arr_config['auth'])) $this->redis->auth($arr_config['auth']);
        if($arr_config['serializer']){
            $this->redis->setOption( \Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP );
            $this->redis->setOption( \Redis::OPT_READ_TIMEOUT,  $arr_config['timeout'] );
        }
    }
}