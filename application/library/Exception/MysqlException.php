<?php
/**
 * User: tangyijun
 * Date: 2019-07-12
 * Time: 15:04
 */
namespace Exception;

class MysqlException extends \PDOException
{
    public $error = [
        '1005' => "无法创建文件",
        '1062' => "字段#key#,数据(#value#)重复",
        '1006' => "无法创建表",
        '1007' => "无法创建数据库",
        '1008' => "数据库已存在",
        '1009' => "无法撤销数据库，数据库不存在",
        '1010' => "撤销数据库时出错",
        '1011' => "撤销数据库时出错",
        '1012' => "删除时出错",
        '1013' => "无法读取系统表中的记录",
        '1028' => "已锁定，拒绝更改",
        '1038' => "内存溢出，重启服务器并再次尝试",
        '1039' => "分类内存溢出，增加服务器的分类缓冲区大小",
        '1041' => "连接过多",
        '1043' => "无法获得该地址给出的主机名",
        '1045' => "拒绝用户'%s'@'%s'访问数据库",
        '1047' => "未选择数据库",
        '1048' => "未知命令",
        '1049' => "列'%s'不能为空",
        '1050' => "未知数据库",
        '1051' => "表已存在",
        '1052' => "未知表",
        '1054' => "在操作过程中服务器关闭",
        '1055' => "未知列",
        '1056' => "不在GROUP BY中",
        '1057' => "无法在'%s'上创建组",
        '1058' => "语句中有sum函数和相同语句中的列",
        '1059' => "列计数不匹配值计数",
        '1060' => "ID名称'%s'过长",
        '1061' => "I重复列名",
        '1064' => "列分类符不正确",
        '1066' => "查询为空",
        '1067' => "非唯一的表/别名",
        '1068' => "关于'%s'的无效默认值",
        '1069' => "定义了多个主键",
        '1070' => "指定了过多键部分：允许的最大键部分是%d",
    ];

    public $err;

    public $code;

    public function __construct(\PDOException $e) {
        if(strstr($e->getMessage(), 'SQLSTATE[')) {
            //返回给客户端报错的信息
            switch ($e->errorInfo[1]){
                case 1062:
                    preg_match_all('/\'(.*?)\'/',$e->getMessage(),$match);
                    $this->err  = str_replace(['#key#','#value#'],[$match[1][1],$match[1][0]],$this->error[$e->errorInfo[1]]);;
                    $this->code = $e->errorInfo[1];
                    break;
                default:
                    break;
            }
            //记录错误日志
            \Tool\Log::getInstance()->write([
                $e->getMessage(), $e->getCode(), $e->file, $e->line,
            ],1,'mysql_error',true);
        }
    }
}