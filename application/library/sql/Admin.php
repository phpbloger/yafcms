<?php
/**
 * User: tangyijun
 * Date: 2019-06-28
 * Time: 15:05
 */
namespace Sql;
class Admin
{
    const SQL_GET_ADMIN_LIST = "SELECT a.`id`,a.`username`,a.`role_id`,a.`last_login_time`,a.`last_login_ip`,b.`role_name`  FROM `admin` AS `a` LEFT JOIN `role` AS `b` on a.`role_id` = b.`id` WHERE a.`id` != 1";

    const SQL_GET_ADMIN_BY_ROLE_ID = "SELECT  `role_id` FROM `admin` WHERE `role_id` = ? LIMIT 1,1";
}