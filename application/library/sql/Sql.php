<?php
/**
 * User: tangyijun
 * Date: 2019-02-21
 * Time: 14:30
 */
namespace Sql;
class Sql
{
    const SQL_LOGIN        = 'SELECT * FROM `admin` WHERE `username` = ? AND `status` = 1 LIMIT 1';

    const SQL_AUTH_INFO    = 'SELECT * FROM `role` WHERE `id` = ?  LIMIT 1';

    const SQL_SYSTEM       = "UPDATE `system` SET `value` = ?  WHERE  `key` = ?";

    const SQL_LOGIN_UPDATE = "UPDATE `admin` SET `last_login_time` = ? , `last_login_ip` = ? WHERE `username` = ?";
}