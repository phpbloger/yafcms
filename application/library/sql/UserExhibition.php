<?php
/**
 * User: tangyijun
 * Date: 2019-07-22
 * Time: 15:15
 */
namespace Sql;
class UserExhibition
{
    const SQL_INSERT_DATA  = "INSERT INTO `user_exhibition` (`ex_id`,`uid`,`ex_position_ids`,`per_tran_price`,`per_dis_price`,`number`,`ex_tran_price`,`ex_dis_price`) VALUES (?,?,?,?,?,?,?,?)";

    const SQL_ROW_BY_EX_ID = "SELECT `id`,`uid`,`ex_id` FROM `user_exhibition` WHERE `ex_id` = ?";
}