<?php
/**
 * User: tangyijun
 * Date: 2019-07-12
 * Time: 11:22
 */
namespace Sql;
class ExhibitionPosition
{
    const SQL_GET_POSITION_BY_EX_ID = "SELECT * FROM `exhibition_position` WHERE `ex_id` = ?";

    const SQL_SAVE_POSITION         = "INSERT INTO `exhibition_position`  (`ex_id`,`position`,`status`,`type`,`size`,`total_price`)  VALUES  (?,?,?,?,?,?)";

    const SQL_DELETE_POSITION       = "DELETE FROM `exhibition_position` WHERE `id` = ?";

    const SQL_UPDATE_TYPE_BY_IDS    = "UPDATE `exhibition_position` SET `type` = ?,`status` = 2 WHERE `id` in (#ids#)";
}