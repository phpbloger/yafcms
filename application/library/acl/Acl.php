<?php
/**
 * User: tangyijun
 * Date: 2019-02-26
 * Time: 15:02
 */
namespace acl;
/**
 * Class Acl
 * @package acl
 * 菜单配置
 * 最多支持三级菜单加载，多余三级菜单无法控制菜单和权限
 */
class Acl extends \Cache\Cache
{
    /**
     * @var array
     * 手动配置菜单栏
     */
    private  $acl_menu = [
        [
            'id'    => '0',
            'title' => '系统设置',
            'icon'  => 'el-icon-setting',
            'path'  => '',
            'child' => [
                [
                    'id'      =>'0-1',
                    'title'   => '管理员管理',
                    'is_menu' => true,
                    'icon'    => '',
                    'path'    => '/admin/index',
                    'child'   => [
                        [
                            'id'      =>'0-1-1',
                            'title'   => '添加',
                            'is_menu' => false,
                            'icon'    => '',
                            'path'    => '/admin/addmodification',
                        ],
                        [
                            'id'      =>'0-1-2',
                            'title'   => '删除',
                            'is_menu' => false,
                            'icon'    => '',
                            'path'    => '/admin/delete',
                        ],
                    ]
                ],
                [
                    'id'      =>'0-2',
                    'title'   => '角色管理',
                    'is_menu' => true,
                    'icon'    => '',
                    'path'    => '/role/index',
                    'child'   => [
                        [
                            'id'      =>'0-2-1',
                            'title'   => '添加/编辑',
                            'is_menu' => false,
                            'icon'    => '',
                            'path'    => '/role/addmodification',
                        ],
                        [
                            'id'      =>'0-2-2',
                            'title'   => '删除',
                            'is_menu' => false,
                            'icon'    => '',
                            'path'    => '/role/delete',
                        ],
                    ]
                ],
                [
                    'id'      =>'0-3',
                    'title'   => '系统设置',
                    'is_menu' => true,
                    'icon'    => '',
                    'path'    => '/system/index',
                    'child'   =>[],
                ]
            ]
        ],
        [
            'id'    => '1',
            'title' => '展会管理',
            'icon'  => 'el-icon-menu',
            'is_menu' => true,
            'path'  => '/exhibition/index',
            'child' => [
                [
                    'id'    => '1-1',
                    'title' => '添加展会类别',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/category/save',
                    'child' => []
                ],
                [
                    'id'    => '1-2',
                    'title' => '删除展会类别',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/category/delete',
                    'child' => []
                ],
                [
                    'id'    => '1-3',
                    'title' => '添加展会标签',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/label/save',
                    'child' => []
                ],
                [
                    'id'    => '1-4',
                    'title' => '删除展会标签',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/label/delete',
                    'child' => []
                ],
                [
                    'id'    => '1-5',
                    'title' => '查看展会详情',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/exhibition/detail',
                    'child' => []
                ],
                [
                    'id'    => '1-6',
                    'title' => '获取展会位置',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/exhibitionposition/index',
                    'child' => []
                ],
                [
                    'id'    => '1-7',
                    'title' => '保存展会位置',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/exhibitionposition/save',
                    'child' => []
                ],
                [
                    'id'    => '1-8',
                    'title' => '添加、修改展位',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/exhibition/save',
                    'child' => []
                ],
                [
                    'id'    => '1-9',
                    'title' => '获取符合条件的展位信息',
                    'icon'  => '',
                    'is_menu' => false,
                    'path'  => '/exhibition/current',
                    'child' => []
                ],
            ]
        ],
        [
            'id'      => '2',
            'title'   => '客户管理',
            'icon'    => 'el-icon-user',
            'is_menu' => true,
            'path'    => '/user/index',
            'child'   => [
                [
                    'id'      => '2-1',
                    'title'   => '添加客户',
                    'icon'    => '',
                    'is_menu' => false,
                    'path'    => '/user/save',
                ],
                [
                    'id'      => '2-2',
                    'title'   => '客户跟进',
                    'icon'    => '',
                    'is_menu' => false,
                    'path'    => '/userremark/save',
                ],
                [
                    'id'      => '2-3',
                    'title'   => '跟进记录',
                    'icon'    => '',
                    'is_menu' => false,
                    'path'    => '/userremark/index',
                ],
                [
                    'id'      => '2-4',
                    'title'   => '预定包名',
                    'icon'    => '',
                    'is_menu' => false,
                    'path'    => '/userexhibition/sign',
                ],
            ]
        ]
    ];

    private static $acl_auth  = [];

    private static $acl_title = [];

    public function __construct()
    {
        foreach ($this->acl_menu as $key => $value){
            self::$acl_title[$value['id']] = $value['title'];
            if($value['path']){
                self::$acl_auth[$value['path']] = [
                    'id'    => $value['id'],
                    'title' => $value['title'],
                ];
            }
            if($value['child']){
                foreach ($value['child'] as $key1 => $value1){
                    self::$acl_title[$value1['id']] = $value1['title'];
                    if($value1['path']){
                        self::$acl_auth[$value1['path']] = [
                            'id'    => $value1['id'],
                            'title' => $value1['title'],
                        ];
                    }
                    if($value1['child']){
                        foreach ($value1['child'] as $key2 => $value2){
                            self::$acl_title[$value2['id']] = $value2['title'];
                            self::$acl_auth[$value2['path']] = [
                                'id'    => $value2['id'],
                                'title' => $value2['title'],
                            ];
                        }
                    }
                }
            }
        }
    }

    public function getAclMenus()
    {
        return $this->acl_menu;
    }

    public function getAclAuth(){
        return self::$acl_auth;
    }

    public function getAclTitle()
    {
        return self::$acl_title;
    }
}