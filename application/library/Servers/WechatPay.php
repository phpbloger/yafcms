<?php
/**
 * User: tangyijun
 * Date: 2018-08-20
 * Time: 15:20
 */
namespace servers;
class WechatPay{
    public $values = [];
    /**
     * @param int $length
     * @return string
     * 生成随机参数
     */
    public function  getNonceStr($length = 32){
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {
            $str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
        }
        return $str;
    }

    /**
     * @param $value
     * @return string
     * 格式化参数
     */
    public function ToUrlParams($value)
    {
        $buff = "";
        foreach ($value as $k => $v)
        {
            if($k != "sign" && $v != "" && !is_array($v)){
                $buff .= $k . "=" . $v . "&";
            }
        }

        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * @param $value
     * @return string
     */
    public function MakeSign($value)
    {
        //签名步骤一：按字典序排序参数
        ksort($value);
        $string = $this->ToUrlParams($value);
        //签名步骤二：在string后加入KEY
        $string = $string . "&key=".\Yaf\Application::app()->getConfig()->wechat->config->key;
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }

    /**
     * @param $xml
     * @param $url
     * @param bool $useCert
     * @param int $second
     * @return mixed
     * @throws \Exception
     */
    protected  function postXmlCurl($xml, $url, $useCert = false, $second = 30){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        if(\Yaf\Application::app()->getConfig()->wechat->config->curl_proxy_host!= "0.0.0.0"
            && \Yaf\Application::app()->getConfig()->wechat->config->curl_proxy_host != 0){
            curl_setopt($ch,CURLOPT_PROXY, \Yaf\Application::app()->getConfig()->wechat->config->curl_proxy_host);
            curl_setopt($ch,CURLOPT_PROXYPORT, \Yaf\Application::app()->getConfig()->wechat->config->curl_proxy_host);
        }
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        if($useCert == true){
            curl_setopt($ch,CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
            curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLCERT,\Yaf\Application::app()->getConfig()->wechat->config->sslcert_path);
            curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
            curl_setopt($ch,CURLOPT_SSLKEY, \Yaf\Application::app()->getConfig()->wechat->config->sslkey_path);
        }
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $data = curl_exec($ch);
        if($data){
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new \Exception("curl出错，错误码:$error",10018);
        }
    }


    /**
     * @return array|string
     * 取毫秒时间
     */
    public static function getMillisecond(){
        //获取毫秒的时间戳
        $time = explode ( " ", microtime () );
        $time = $time[1] . ($time[0] * 1000);
        $time2 = explode( ".", $time );
        $time = $time2[0];
        return $time;
    }

    /**
     * @param string $trade_type
     * @return array
     * @throws \Yaf\Exception
     */
    public function wechatPay($trade_type = 'NATIVE'){
        //通用参数
        $this->values['appid']         = \Yaf\Application::app()->getConfig()->wechat->config->appid;
        $this->values['mch_id']        = \Yaf\Application::app()->getConfig()->wechat->config->mchid;
        $this->values['nonce_str']     = $this->getNonceStr();
        $this->values['time_start']    = date("YmdHis");
        if('JSAPI' === $this->values['trade_type']){
            if(empty($this->values['openid']) || !isset($this->values['openid']))
                throw new \Yaf\Exception('jsAPI支付openid为必传项',-1);
        }
        if('NATIVE' === $this->values['trade_type']){
            $this->values['spbill_create_ip'] = $_SERVER['REMOTE_ADDR']; //获取机器的IP
            if(empty($this->values['product_id'] || !isset($this->values['product_id']))){
                throw new \Yaf\Exception('NATIVE支付商品id为必传项',-1);
            }
        }
        $sign = $this->MakeSign($this->values);
        $this->values['sign'] = $sign;
        $xml = ToXml($this->values);//转xml对象
        $response = $this->postXmlCurl($xml, \Yaf\Application::app()->getConfig()->wechat->config->pay_url, false, 6);//发起post请求
        $this->values = FromXml($response);
        if($this->MakeSign($this->values) !== $this->values['sign']){
            throw new \Yaf\Exception('签名错误',-1);
        }
        if('SUCCESS' == $this->values['return_code'] && $trade_type == 'NATIVE'){
            if($this->values['result_code'] == 'SUCCESS'){
                return [
                    'trade_type' => $trade_type,
                    'code_url' => $this->values['code_url']
                ];
            }else{
                throw new \Yaf\Exception($this->values['err_code_des'],-3);
            }
        }elseif('JSAPI' == $trade_type && 'SUCCESS' == $this->values['return_code']){
            $timeStamp = time();
            $nonceStr = $this->getNonceStr();
            $sign = $this->MakeSign([
                'appId'     => $this->values['appid'],
                'timeStamp' => "$timeStamp",
                'nonceStr'  => $nonceStr,
                'package'   => 'prepay_id='.$this->values['prepay_id'],
                'signType'  => 'MD5',
            ]);
            return [
                'appId'      => $this->values['appid'],
                'nonceStr'   => $nonceStr,
                'timeStamp'  => $timeStamp,
                'package'    => 'prepay_id='.$this->values['prepay_id'],
                'signType'   => 'MD5',
                'paySign'    => $sign
            ];
        }else{
            throw new \Exception('不支持的支付方式',-1);
        }
    }

    /**
     * @return array|mixed
     * @throws \Yaf\Exception
     * 微信退款
     */
    public function refund(){
        //通用参数
        $this->values['appid']         = \Yaf\Application::app()->getConfig()->wechat->config->appid;
        $this->values['mch_id']        = \Yaf\Application::app()->getConfig()->wechat->config->mchid;
        $this->values['nonce_str']     = $this->getNonceStr();
        $sign = $this->MakeSign($this->values);
        $this->values['sign'] = $sign;
        $xml = ToXml($this->values);//转xml对象
        $response = $this->postXmlCurl($xml,"https://api.mch.weixin.qq.com/secapi/pay/refund", true, 6);//发起post请求
        $this->values = FromXml($response);
        if($this->MakeSign($this->values) !== $this->values['sign']){
            throw new \Yaf\Exception('签名错误',-1);
        }
        return $this->values;
    }
}