<?php
/**
 * User: tangyijun
 * Date: 2018-11-12
 * Time: 15:47
 */
namespace servers;

class Sms
{
    protected static $instance = null;
    public static function get_instance()
    {
        if(self::$instance == null){
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param $token
     * @param $to
     * @param string $action
     * @return array|mixed|string
     * @throws \Yaf\Exception
     */
    public function sendCode($token,$to,$action = 'register')
    {
        $ip = Ip::get_client_ip();
        $redis_token = Redis::get_instence()->get($ip);
        if($redis_token != $token){
            throw new \Yaf\Exception('token错误',-2);
        }
        $last_send_time = Redis::get_instence()->get($ip.'send_time');
        if(time() - $last_send_time < 60){
            return '发送频繁';
        }
        $last_code  = Redis::get_instence()->get($to.'-'.$action);
        if(!empty($last_code)){
            return '请不要重复发送短信';
        }
        $accountSid = 'fd75804114b24a3ab1802cb6f7bb2dbb';
        $token      = 'd920896ea4f84d9abac945da127b30f3';
        $time = time();
        $time = date('YmdHis',$time);
        $sig  = md5($accountSid.$token.$time);
        $code = rand(1000,9999);
        $smsContent = "【企凡网】您的验证码为{$code}，请于15分钟内正确输入，如非本人操作，请忽略此短信。";
        $ch = curl_init();
        /* 设置验证方式 */
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept:text/plain;charset=utf-8', 'Content-Type:application/x-www-form-urlencoded','charset=utf-8'));
        /* 设置返回结果为流 */
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        /* 设置超时时间*/
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        /* 设置通信方式 */
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // 发送短信
        $data = array('accountSid'=>$accountSid,'smsContent'=>$smsContent,'to'=>$to,'timestamp'=>$time,'smsContent'=>$smsContent,'sig'=>$sig);
        curl_setopt($ch, CURLOPT_URL, 'https://api.miaodiyun.com/20150822/industrySMS/sendSMS');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        $json_data = curl_exec($ch);
        //将数据转换成数组
        $arr = json_decode($json_data,true);
        if($arr['respCode']=='00000'){
            //发送成功，记录该IP今日发送册数
            Redis::get_instence()->set($ip.'send_time',time(),900);
            Redis::get_instence()->set($to.'-'.$action,$code,900);
            //重新生成token
            $token = $this->setToken($ip);
            return  [
                'code'   => true,
                'token'  => $token
            ];
        }else{
            $msg = [
                '00022'=>'操作频繁',
                '00021' => '子账号余额不足',
                '00029' => '服务器异常',
                '00023' => '开发者余额不足',
                '00039' => '验证码为空或者缺少此参数'
            ];
            $str = empty($msg[$arr['respCode']]) ? '操作失败': $msg[$arr['respCode']];
            return $str;
        }
    }

    public function setToken($ip){
        $token = md5(md5('woyaofasongyitiaoduanxin').'1992');
        Redis::get_instence()->set($ip,$token,900);
        return $token;
    }
}