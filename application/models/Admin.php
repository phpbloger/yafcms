<?php
/**
 * User: tangyijun
 * Date: 2019-06-26
 * Time: 17:20
 */
class AdminModel extends AbstractModel
{
    public $field = [];
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function select()
    {
        return \PdoConnect::getInstance()->fetchAll(\Sql\Admin::SQL_GET_ADMIN_LIST,[]);
    }

    public function getRowByRoleId(int $role_id) : array
    {
        return \PdoConnect::getInstance()->fetch(\Sql\Admin::SQL_GET_ADMIN_BY_ROLE_ID,[$role_id]);
    }

    public function add(array  $data = [])
    {
        $this->field = $data;
        return parent::add();
    }

    public function update($id,array  $data = [])
    {
        $this->field = $data;
        return parent::update($id);
    }

    public function delete($id)
    {
        return parent::delete($id);
    }
}