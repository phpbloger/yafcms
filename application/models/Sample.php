<?php
/**
 * @name SampleModel
 * @desc sample数据获取类, 可以访问数据库，文件，其它系统等
 * @author laptop-ia2ndp3r\11716
 */
class SampleModel  extends AbstractModel {

    /**
     * SampleModel constructor.
     */
    public function __construct() {
        parent::__construct();
    }   
    
    public function selectSample() {
        return \Query\Table::instance('sample')->read([
            'where'  => [
                'create_time' => [
                    '>' => [1550653184],
                    '<' => [1550720452]
                ]
            ],
            'column' => ['create_time','update_time']
        ]);
    }

    public function insertSample() {
        return $this->add();
    }


    public function  updateSample($id){
        return $this->update($id);
    }
}
