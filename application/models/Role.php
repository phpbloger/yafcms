<?php
/**
 * User: tangyijun
 * Date: 2019-06-26
 * Time: 17:20
 */
class RoleModel extends AbstractModel
{
    public function __construct()
    {
        parent::__construct();
    }

    public function select()
    {
        return \Query\Table::getInstance('role')->read([]);
    }

    public function add()
    {
        return parent::add();
    }

    public function update($id)
    {
        return parent::update($id);
    }

    public function delete($id)
    {
        return parent::delete($id);
    }
}