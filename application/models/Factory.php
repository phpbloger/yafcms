<?php
/**
 * User: tangyijun
 * Date: 2019-07-03
 * Time: 18:07
 */
class FactoryModel extends AbstractModel {

    public $table = null;

    public $filed = [];

    public $update_time = true;

    /**
     * FactoryModel constructor.
     * @param $table
     * @param array $data
     */
    public function __construct($table,$data = [])
    {
        $this->table = $table;
        $this->field = $data;
        parent::__construct();
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param array $column
     * @param array $where
     * @param array $order
     * @return mixed
     */
    public function select($offset = 0,$limit = 0,$column = [],$where = [],$order = [])
    {
        return \Query\Table::getInstance($this->table)->read([
            'offset' => $offset,
            'limit'  => $limit,
            'column' => $column,
            'where'  => $where,
            'order'  => $order
        ]);
    }

    public function count($where = '')
    {
        return parent::count($where = '');
    }

    public function add()
    {
        return parent::add();
    }

    public function update($id)
    {
        return parent::update($id);
    }

    public function delete($id)
    {
        return parent::delete($id);
    }
}