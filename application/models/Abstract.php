<?php
/**
 * User: tangyijun
 * Date: 2019-02-13
 * Time: 17:02
 */
abstract class  AbstractModel
{
    public $field = [];
    public $table = "";
    public $update_time = false;
    public function __construct()
    {
        if(empty($this->table)){
            $this->table = strtolower(substr(get_called_class(),0,-5));
        }
        if(empty($this->field)){
            $data   = \PdoConnect::getInstance()->fetchAll('DESC ' . $this->table);
            $fields = array_combine(array_column($data, 'Field'), $data);
            foreach ($fields as $field => $option){
                $this->field[$field] = filter_input(INPUT_POST,$field);
            }
        }
        if($this->update_time === true){
            $this->field['update_time'] = date('Y-m-d H:i:s');
        }
    }

    public function add()
    {
        try {
            $keys = array_keys($this->field);
            PdoConnect::getInstance()->pdo->beginTransaction();
            $result = PdoConnect::getInstance()->insert(
                'INSERT INTO `'.$this->table.'`(`'.join('`,`', $keys).'`) VALUES (:'.join(',:', $keys).');', $this->field
            );
            return  PdoConnect::getInstance()->pdo->commit() ? $result['lastInsertId'] : 0;
        } catch (\PDOException $e) {
            PdoConnect::getInstance()->pdo->rollBack();
            throw new \Yaf\Exception($e->getMessage());
        }
        return 0;
    }

    /**
     * @param $id
     * @param array $data
     * @return bool
     * @throws Exception
     * 根据id更新数据的方法
     */
    public function update($id)
    {
        try {
            PdoConnect::getInstance()->pdo->beginTransaction();
            unset($this->field['id']);
            $keys = '';
            foreach ($this->field as $key => $value) {
                $keys .= '`'.$key.'`=:'.$key.',';
            }
            PdoConnect::getInstance()->update(
                'UPDATE `'.$this->table.'` SET '.substr($keys, 0, -1).' WHERE `id`='.$id, $this->field
            );
            return PdoConnect::getInstance()->pdo->commit();
        } catch (Exception $e) {
            PdoConnect::getInstance()->pdo->rollBack();
            throw new Exception($e->getMessage());
        }
        return false;
    }

    /**
     * @param $id
     * @return mixed
     * 根据id删除数据的方法
     */
    public function delete($id)
    {
        return PdoConnect::getInstance()->delete('DELETE FROM `'.$this->table.'` WHERE `id`=?', [$id]);
    }

    /**
     * @param string $where
     * @return mixed
     * 统计数据
     */
    public function count($where = "")
    {
        if($where){
            return PdoConnect::getInstance()->fetch('SELECT COUNT(*) as `total` FROM `'.$this->table.'` WHERE '.$where);
        }
        return PdoConnect::getInstance()->fetch('SELECT COUNT(*) as `total` FROM `'.$this->table.'`');
    }
}