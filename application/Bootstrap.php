<?php
/**
 * @name Bootstrap
 * @author laptop-ia2ndp3r\11716
 * @desc 所有在Bootstrap类中, 以_init开头的方法, 都会被Yaf调用,
 * @see http://www.php.net/manual/en/class.yaf-bootstrap-abstract.php
 * 这些方法, 都接受一个参数:Yaf_Dispatcher $dispatcher
 * 调用的次序, 和申明的次序相同
 */
class Bootstrap extends Yaf\Bootstrap_Abstract{

    public $arr_config = [];

    public function _initConfig() {
		//把配置保存起来
		$this->arr_config = Yaf\Application::app()->getConfig();
		Yaf\Registry::set('config', $this->arr_config);
	}

	public function _initPlugin(Yaf\Dispatcher $dispatcher) {
		//注册一个插件
		$objSamplePlugin = new SamplePlugin();
		$dispatcher->registerPlugin($objSamplePlugin);
	}

	public function _initRoute(Yaf\Dispatcher $dispatcher) {
		//在这里注册自己的路由协议,默认使用简单路由
	}
	
	public function _initView(Yaf\Dispatcher $dispatcher){
		//在这里注册自己的view控制器，例如smarty,firekylin
	}

    /**
     * 设置错误级别
     */
    public function _initError()
    {
        error_reporting(E_ALL);
    }

    /**
     * @param \Yaf\Dispatcher $dispatcher
     * 多域名配置
     */
    public function _initDomainCheck(\Yaf\Dispatcher $dispatcher)
    {
        $domain = $this->arr_config->domain;
        foreach ($domain as $key => $value){
            if( $dispatcher->getRequest()->getServer('SERVER_NAME') == $value['path']){
                $dispatcher->getRequest()->setModuleName(ucfirst($key));
            }
        }
    }
}
