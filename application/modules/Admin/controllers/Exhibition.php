<?php
/**
 * User: tangyijun
 * Date: 2019-07-01
 * Time: 14:27
 */

/**
 * Class Exhibition
 * 展会管理
 */
class ExhibitionController extends \core\Admin\AbstractController
{
    public $model;
    public function init()
    {
       parent::init();
       $this->model = new FactoryModel('exhibition');
    }

    public function indexAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $category_model = new CategoryModel();
            //数据分页
            $page = $this->getRequest()->getPost('page');
            $page = $page ? ($page - 1) * 10 : 0;
            return \Tool\Common::getInstance()->success([
                'category' => $category_model->select(),
                'list'     => $this->model->select($page,10),
                'total'    => $this->model->count()['total']
            ]);
        }
    }

    /**
     * 当前符合条件的展会信息
     */
    public function currentAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            return \Tool\Common::getInstance()->send($this->model->select(
                0, 0, ['id','title','ex_start_time','ex_end_time'],
                ['ex_start_time' => [
                    '>' => [date('Y-m-d H:i:s')]
                ]]
            ));
        }
    }

    public function saveAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $id = $this->getRequest()->getPost('id');
            if($id){
                return \Tool\Common::getInstance()->send($this->model->update($id));
            }
            return \Tool\Common::getInstance()->send($this->model->add());
        }
    }

    /**
     * 删除展会信息
     */
    public function deleteAction()
    {
        return \Tool\Common::getInstance()->send($this->model->delete($this->getRequest()->getPost('id')));
    }

    /**
     * 展会详情
     */
    public function detailAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $id = $this->getRequest()->getPost('id');
            $row = \PdoConnect::getInstance()->fetch(\Sql\Exhibition::SQL_GET_ROW,[$id]);
            return \Tool\Common::getInstance()->success($row);
        }
    }
}