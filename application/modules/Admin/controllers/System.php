<?php
/**
 * User: tangyijun
 * Date: 2019-07-01
 * Time: 9:56
 */
class  SystemController extends \core\Admin\AbstractController
{
    public function indexAction()
    {
        if($this->getRequest()->isPost()){
            $config = \Query\Table::getInstance('system')->read();
            $res = [];
            foreach ($config as $k => $v){
                $res[$v['key']] = $v['value'];
            }
            return \Tool\Common::getInstance()->success($res);
        }
    }

    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        foreach ($data as $key => $value){
            $res = \PdoConnect::getInstance()->update(\Sql\Sql::SQL_SYSTEM,[$value,$key]);
        }
        return \Tool\Common::getInstance()->send(true);
    }
}