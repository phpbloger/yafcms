<?php
/**
 * User: tangyijun
 * Date: 2019-02-22
 * Time: 14:27
 */
class LoginController extends \Yaf\Controller_Abstract
{
    public function indexAction()
    {
        return true;
    }

    public function loginAction()
    {
        $username    = $this->getRequest()->getPost('username');
        $password    = $this->getRequest()->getPost('password');
        $remember_me = $this->getRequest()->getPost('remember_me');

        $login = \PdoConnect::getInstance()->fetch(
           \Sql\Sql::SQL_LOGIN, [$username]
        );
        $expire = $remember_me == 'true' ? 86400 * 3 : 1440;

        if ($login && password_verify($password,$login['password'])) {
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
            $login_time = date('Y-m-d H:i:s');

            $res = \PdoConnect::getInstance()->update(
               \Sql\Sql::SQL_LOGIN_UPDATE,[$login_time,$ip,$username]
            );
            if($res){
                \Tool\Session::getInstance()->set('login_info', [
                    'id'              => $login['id'],
                    'username'        => $login['username'],
                    'last_login_time' => $login_time,
                    'last_login_ip'   => $ip,
                    'role_id'         => $login['role_id']
                ],$expire);
                \Tool\Common::getInstance()->success([],'恭喜，登录成功');
            }

        }
         \Tool\Common::getInstance()->error('login err !');
    }

    public function logoutAction()
    {
        $res = \Yaf\Session::getInstance()->del('login_info');
        return \Tool\Common::getInstance()->send($res);
    }
}