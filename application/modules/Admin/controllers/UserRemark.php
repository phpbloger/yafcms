<?php
/**
 * User: tangyijun
 * Date: 2019-07-16
 * Time: 12:03
 */
class UserRemarkController extends \Core\Admin\AbstractController
{
    public $model = null;

    public function init()
    {
        parent::init();
        $this->model = new FactoryModel('user_remark');
    }

    public function indexAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $uid  = $this->getRequest()->getPost('uid');
            return \Tool\Common::getInstance()->send([
                'remark' => $this->model->select(0,0,[],['uid' => $uid],['update_time' => 'desc']),
            ]);
        }
    }

    /**
     * 保存跟进记录
     */
    public function saveAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $id = $this->getRequest()->getPost('id');
            if(!$id){
               return \Tool\Common::getInstance()->send($this->model->add());
            }
            return \Tool\Common::getInstance()->send($id);
        }
    }
}