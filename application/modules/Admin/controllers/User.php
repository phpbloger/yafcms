<?php
/**
 * User: tangyijun
 * Date: 2019-07-10
 * Time: 14:11
 */
class UserController extends \Core\Admin\AbstractController
{
    public $model = null;

    public function init()
    {
        parent::init();
        $this->model = new FactoryModel('user');
    }

    /**
     * 用户首页
     */
    public function indexAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $role_id = $this->login_info['role_id'];
            $page = $this->getRequest()->getPost('page');
            $page = $page ? ($page - 1) * 10 : 0;
            $where = [];
            $count_where = "";
            if($role_id != 0){
                $where = [
                    'admin_id' => $this->login_info['id']
                ];
                $count_where = "admin_id = {$this->login_info['id']}";
            }
            return \Tool\Common::getInstance()->success([
                'list'     => $this->model->select($page,10,[],$where),
                'total'    => $this->model->count($count_where)['total']
            ]);

        }
    }

    /**
     * 添加客户数据
     */
    public function saveAction()
    {
        if($this->getRequest()->isXmlHttpRequest() && $this->getRequest()->isPost()){
            $uid = $this->getRequest()->getPost('uid');
            if(!$uid){
                return \Tool\Common::getInstance()->send($this->model->add());
            }
            return \Tool\Common::getInstance()->send($this->model->update($uid));
        }
    }
}