<?php
/**
 * User: tangyijun
 * Date: 2019-07-22
 * Time: 14:31
 */
class UserExhibitionController extends \Core\Admin\AbstractController
{
    public function signAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $data = $this->getRequest()->getPost();
            $row  = \PdoConnect::getInstance()->fetch(\Sql\UserExhibition::SQL_ROW_BY_EX_ID,[$data['ex_id']]);
            if($row && !empty($row)){
                return \Tool\Common::getInstance()->error('该展会已经包名了，请不要重复包名');
            }
            \PdoConnect::getInstance()->insert(\Sql\UserExhibition::SQL_INSERT_DATA,[
                $data['ex_id'],
                $data['uid'],
                $data['ex_position_ids'],
                $data['per_tran_price'],
                $data['per_dis_price'],
                $data['number'],
                $data['ex_tran_price'],
                $data['ex_dis_price'],
            ]);
            $parameter = explode(',',$data['ex_position_ids']);
            $prepare   = rtrim( str_pad('?', 2 * count($parameter), ',?') , ',');
            array_unshift($parameter,'success');
            return \Tool\Common::getInstance()->send(
                \PdoConnect::getInstance()->update(str_replace('#ids#',$prepare,\Sql\ExhibitionPosition::SQL_UPDATE_TYPE_BY_IDS),$parameter)
            );
        }
    }
}