<?php
/**
 * User: tangyijun
 * Date: 2019-02-25
 * Time: 10:03
 */
class IndexController extends \core\Admin\AbstractController
{

    public function indexAction()
    {
        $system_info = $this->getRequest()->getServer();
        $this->_view->assign([
            'system_info' => $system_info ,
            'login_info'  => $this->login_info
        ]);
        return true;
    }

    public function permissionAction(){
        return true;
    }
}