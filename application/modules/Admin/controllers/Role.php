<?php
/**
 * User: tangyijun
 * Date: 2019-06-26
 * Time: 16:30
 */
class RoleController extends \core\Admin\AbstractController
{
    public $model;

    public function init()
    {
        parent::init();
        $this->model = new RoleModel();
    }

    public function indexAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            return \Tool\Common::getInstance()->success([
                'role' => $this->model->select(),
                'auth' => \acl\Acl::getInstance()->getAclMenus()
            ]);
        }
    }

    public function addModificationAction()
    {
        $id = $this->getRequest()->getPost('id');
        if($id){
            return \Tool\Common::getInstance()->send($this->model->update($id));
        }
        return \Tool\Common::getInstance()->send($this->model->add());
    }

    public function deleteAction()
    {
        $id   = $this->getRequest()->getPost('id');
        $role_model = new AdminModel();
        $role = $role_model->getRowByRoleId($id);
        if(!empty($role)){
            \Tool\Common::getInstance()->error('该角色有绑定管理员，不能删除',-1);
        }
        return \Tool\Common::getInstance()->send($this->model->delete($this->getRequest()->getPost('id')));
    }

}