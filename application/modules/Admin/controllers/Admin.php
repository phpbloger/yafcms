<?php
/**
 * User: tangyijun
 * Date: 2019-06-26
 * Time: 14:15
 */
class AdminController extends \Core\Admin\AbstractController
{
    public $model;

    public function init()
    {
        parent::init();
        $this->model = new AdminModel();
    }

    public function indexAction()
    {
        $role_model = new RoleModel();
        if($this->getRequest()->isPost()){
            return \Tool\Common::getInstance()->success([
                'role' => $role_model->select(),
                'list' => $this->model->select()
            ]);
        }
    }

    public function addModificationAction()
    {
        $id       = $this->getRequest()->getPost('id');
        $username = $this->getRequest()->getPost('username');
        $password = password_hash($this->getRequest()->getPost('username'),true);
        $role_id  =  $this->getRequest()->getPost('role_id');
        if($id){
            \Tool\Common::getInstance()->send($this->model->update($id,[
                'username' => $username,
                'password' => $password,
                'role_id' => $role_id
            ]));
        }
        \Tool\Common::getInstance()->send($this->model->add([
            'username' => $username,
            'password' => $password,
            'role_id'  => $role_id,
            'last_login_time' =>date('Y-m-d H:i:s'),
            'last_login_ip'   => $this->getRequest()->getServer('REMOTE_ADDR')
        ]));
    }

    public function deleteAction()
    {
        \Tool\Common::getInstance()->send($this->model->delete($this->getRequest()->getPost('id')));
    }
}