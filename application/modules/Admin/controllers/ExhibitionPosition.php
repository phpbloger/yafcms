<?php
/**
 * User: tangyijun
 * Date: 2019-07-12
 * Time: 11:18
 */
class ExhibitionPositionController extends \Core\Admin\AbstractController
{
    public function indexAction()
    {
       if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
           $ex_id = $this->getRequest()->getPost('ex_id');
           return \Tool\Common::getInstance()->send([
               'current_ex' => \PdoConnect::getInstance()->fetchAll(\Sql\ExhibitionPosition::SQL_GET_POSITION_BY_EX_ID,[$ex_id]),
               'current_row'=> \PdoConnect::getInstance()->fetch(\sql\Exhibition::SQL_GET_ROW,[$ex_id])
           ]);
       }
    }

    /**
     * 添加位置
     */
    public function saveAction()
    {
        if($this->getRequest()->isPost() && $this->getRequest()->isXmlHttpRequest()){
            $data = $this->getRequest()->getPost();
            return \Tool\Common::getInstance()->send(
                \PdoConnect::getInstance()->insert(\Sql\ExhibitionPosition::SQL_SAVE_POSITION,[
                    $data['ex_id'],
                    $data['position'],
                    $data['status'],
                    $data['type'],
                    $data['size'],
                    $data['total_price'],
                ])
            );
        }
    }
}