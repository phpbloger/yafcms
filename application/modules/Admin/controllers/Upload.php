<?php
/**
 * User: tangyijun
 * Date: 2018-10-09
 * Time: 16:10
 */
class UploadController extends Yaf\Controller_Abstract{

    /**
     * @return string
     */
    public function indexAction(){
        $file = new \Tool\File();
        $ret = $file->move('public/uploads',[
            'ext'  => ['jpg','png','jpeg','gif'],
            'size' => 2000000
        ],'file');
        if(!$ret){
            return \Tool\Common::getInstance()->error($file->getError());
        }
       return \Tool\Common::getInstance()->success([
           'file_path' => $ret
       ]);
    }

    /**
     * 富文本编辑器文件上传的方法【以富文本编辑器格式返回参数】
     */
    public function xhEditorUploadAction()
    {
        header('Content-Type:application/json; charset=utf-8');
        $file = new \Tool\File();
        $ret = $file->move('public/uploads',[
            'ext'  => ['jpg','png','jpeg','wmv','avi','wma','mp3','mid'],
            'size' => 2000000
        ],'image');
        if(!$ret){
             exit(json_encode([
                'err' => $file->getError(),
            ] ));
        }
        exit(json_encode([
            'err' => '',
            'msg' => '!'.$ret
        ]));
    }
}