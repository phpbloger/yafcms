<?php
/**
 * User: tangyijun
 * Date: 2019-07-01
 * Time: 14:35
 */
class LabelController extends \core\Admin\AbstractController
{
    public $model = null;

    public function init()
    {
        parent::init();
        $this->model = new LabelModel();
    }

    public function saveAction()
    {
        return \Tool\Common::getInstance()->send($this->model->add());
    }

    public function deleteAction()
    {
        return \Tool\Common::getInstance()->send($this->model->delete($this->getRequest()->getPost('id')));
    }
}