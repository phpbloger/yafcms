<?php
/**
 * User: tangyijun
 * Date: 2019-02-18
 * Time: 17:19
 */

namespace Core\Admin;

use acl\Acl;

abstract class AbstractController extends \Yaf\Controller_Abstract
{
    protected $login_info = [];

    protected $auth  = [];

    protected static $menus = [];

    private $current_menus = 1;

    public $controller = null;

    public $action = null;

    public function init()
    {
        $this->login_info =  \Tool\Session::getInstance()->get('login_info');
        if(empty($this->login_info)) $this->redirect('/login/index');
        if($this->login_info['role_id'] != 0){
            $acc_info = \PdoConnect::getInstance()->fetch(
                \Sql\Sql::SQL_AUTH_INFO, [$this->login_info['role_id']]
            );
            $this->auth = json_decode($acc_info['auth_ids'],true);
            //需要判断父节点是否在数组中，如果不在需要将父节点加入到数组中
            foreach ($this->auth as $v){
                $parent = explode('-',$v);
                if(!in_array($parent[0],$this->auth) && count($parent) == 2){
                    $this->auth[] = $parent[0];
                }
                if(!in_array($parent[0].'-'.$parent[1],$this->auth) && count($parent) == 3){
                    $this->auth[] = $parent[0].'-'.$parent[1];
                }
            }
        }
        self::$menus = Acl::getInstance()->getAclMenus();
        $acl_auth    = Acl::getInstance()->getAclAuth();
        $acl_title   = Acl::getInstance()->getAclTitle();
        $this->controller  = strtolower($this->getRequest()->getControllerName());
        $this->action      = strtolower($this->getRequest()->getActionName());
        $auth        = '/'.$this->controller.'/'.$this->action;
        $this->current_menus = $acl_auth[$auth]['id'];
        $this->_view->assign([
            'menus_html'   => $this->getMenus(),
            'current_menu' => [$acl_title[explode('-',$this->current_menus)[0]],$acl_title[$this->current_menus]],
            'login_info'   => $this->login_info,
            'controller'   => $this->controller,
            'action'       => $this->action
        ]);
        if($this->controller == 'index' || $this->login_info['role_id'] == 0) return true;
        if(!in_array($this->current_menus,$this->auth)){
            if($this->getRequest()->isXmlHttpRequest()){
                header('Content-Type:application/json; charset=utf-8');
                exit(json_encode([
                    'msg'  => '没有权限操作',
                    'code' => -1
                ]));
            }else{
                $this->redirect('/index/permission');
                die('没有权限操作');
            }
        }
    }

    /**
     * @return string
     * 获取符合权限的菜单
     */
    protected function getMenus()
    {
        if($this->login_info['id'] != 1){
            foreach (self::$menus as $key => $value){
                if(!in_array($value['id'],$this->auth)){
                    unset(self::$menus[$key]);
                }
                if(!empty($value['child'])){
                    foreach ($value['child'] as $k => $v){
                        if(!in_array($v['id'],$this->auth) || $v['is_menu'] == false){
                            unset(self::$menus[$key]['child'][$k]);
                        }
                        if(!empty($v['child'])){
                            foreach ($v['child'] as $k1 => $v1){
                                if(!in_array($v1['id'],$this->auth) || $v1['is_menu'] == false){
                                    unset(self::$menus[$key]['child'][$k]['child'][$k1]);
                                }
                            }
                        }
                    }
                }
            }
        }else{
            foreach (self::$menus as $key => $value){
                if(!empty($value['child'])){
                    foreach ($value['child'] as $k => $v){
                        if($v['is_menu'] == false){
                            unset(self::$menus[$key]['child'][$k]);
                        }
                        if(!empty($v['child'])){
                            foreach ($v['child'] as $k1 => $v1){
                                if($v1['is_menu'] == false){
                                    unset(self::$menus[$key]['child'][$k]['child'][$k1]);
                                }
                            }
                        }
                    }
                }
            }
        }
        $menus_html = '<el-menu default-active="'.($this->current_menus).'" class="el-menu-vertical-demo"  :collapse="isCollapse"  background-color="#20222A" text-color="#fff" active-text-color="#ffd04b">';
        foreach (self::$menus as $menu_key => $menu_va){
            if($menu_va['child']){
                $menus_html .= '<el-submenu index="'.$menu_va['id'].'">';
                $menus_html .= '<template slot="title"> <i class="'.$menu_va['icon'].'"></i><span>'.$menu_va['title'].'</span></template>';
                foreach ($menu_va['child'] as $key => $value){
                    if($value['child']){
                        $menus_html .= ' <el-submenu index="'.$value['id'].'">';
                        $menus_html .= '<template slot="title">'.$value['title'].'</template>';
                        foreach ($value['child'] as $key1 => $value1){
                            $menus_html .= '<a href="'.$value1['path'].'">';
                            $menus_html .= '<el-menu-item index="'.$value1['id'].'">'.$value1['title'].'</el-menu-item>';
                            $menus_html .= '</a>';
                        }
                        $menus_html .= '</el-submenu>';
                    }else{
                        $menus_html .= '<a href="'.$value['path'].'">';
                        $menus_html .= '<el-menu-item index="'.$value['id'].'">';
                        $menus_html .= '<span slot="title">'.$value['title'].'</span>';
                        $menus_html .= '</el-menu-item>';
                        $menus_html .= '</a>';
                    }
                }
                $menus_html .= '</el-submenu>';
            }else{
                $menus_html .= '<a href="'.$menu_va['path'].'">';
                $menus_html .= '<el-menu-item index="'.$menu_key.'">';
                $menus_html .= '<i class="'.$menu_va['icon'].'"></i>';
                $menus_html .= '<span slot="title">'.$menu_va['title'].'</span>';
                $menus_html .= '</el-menu-item>';
                $menus_html .= '</a>';
            }
        }
        $menus_html.= ' </el-menu>';
        return $menus_html;
    }
}